const bcrypt = require('bcryptjs');

const helpers = {};

// helper encryptPass 
// encrypt password for signup users
helpers.encryptPass = async (password) => {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);
    return hash; 
};

// helper comparePass
// compare password for register users
helpers.comparePass = async (password, savedPassword) => {
    try {
        return await bcrypt.compare(password, savedPassword);
    } catch (e) {
        console.log(e);
    }
};

module.exports = helpers;